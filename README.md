
### Задание 2. Дебаг. 

Дана программа [file_manager.php](https://github.com/vitovtnet/test-tasks/blob/master/file_manager.zip) которая работает как файл - менеджер (показывает, какие файлы и каталоги находятся в одной директории с программой). Однако, по неизвестным причинам, скрипт в файле file_manager.php начинает очень сильно тормозить при большом количестве файлов (больше 20). Необходимо определить, какая функция работает медленно, и точно указать, что же так негативно влияет на время работы программы. 
Объясните, как было найдено решение.   


---------------------------------------------

Посмотрев на код скрипта, я решил попробовать его деобфуцировать, чтобы увидеть реальные названия переменных и упростить понимание логики работы.
Был создан скрипт deobfuscate.php
Результат работы раюоты деобфускатора fm.php
http://joxi.ru/Y2LEQl1u98YlGA
дал понимание, что наименования переменных не осмысленные.

далее, я настроил и запустил профилирование с xdebug,
в итоге получив отчет cachegrind.out

Анализатор проявил проблемный код

http://joxi.ru/a2XGnlju179Ylm

метод callback класса list_files_in_folder_callback
содержит задержку выполнения в 1 секунду на каждом файле в цикле. Чем больше файлов в каталоге, который проходит скрипт, тем пропорционально больше общая задержка. Это причина замедления. 


Затрачено 20 минут.




